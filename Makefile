.PHONY : execute execute-v test chocolatey run-tags run-tags-v list-tags list-vars setup apt pip reqs store-password githook decrypt encrypt

# Run the playbook (Assumes 'make setup' has been run)
execute:
	@ansible-playbook --vault-password-file ~/.ansible/password main.yml

execute-v:
	@ansible-playbook -vvv --vault-password-file ~/.ansible/password main.yml

# Run the test task for testing in
test:
	@ansible-playbook --tags "test_task" -e "test_task=true" --vault-password-file ~/.ansible/password main.yml

# Run the test task for testing in
chocolatey:
	@ansible-playbook --tags "chocolatey" -e "chocolatey=true" --vault-password-file ~/.ansible/password main.yml

# Run only the tags passed in separated by comma (e.g. make run-tags update_compose,logrotate)
ifeq (run-tags, $(firstword $(MAKECMDGOALS)))
  runargs := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
  $(eval $(runargs):;@true)
endif
run-tags:
	@ansible-playbook --tags $(runargs) --vault-password-file ~/.ansible/password main.yml

# VERBOSE - Run only the tags passed in separated by comma (e.g. make run-tags update_compose,logrotate)
ifeq (run-tags-v, $(firstword $(MAKECMDGOALS)))
  runargs := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
  $(eval $(runargs):;@true)
endif
run-tags-v:
	@ansible-playbook -vvv --tags $(runargs) --vault-password-file ~/.ansible/password main.yml

# List the available tags that you can run standalone from the playbook
list-tags:
	@grep 'tags:' main.yml | grep -v always | awk -F\" '{print $$2}'

# List variables
ifeq (list-vars, $(firstword $(MAKECMDGOALS)))
  extrafiles := $(wordlist 2, $(words $(MAKECMDGOALS)), $(MAKECMDGOALS))
  $(eval $(extrafiles):;@true)
endif
list-vars:
	@./bin/vars_list.py group_vars/all.yml vars/default_config.yml vars/config.yml inventory $(extrafiles)

# Setup entire environment
setup: apt pip reqs store-password githook

# Install required package for ssh (assumes debiam/ubuntu)
apt:
	@sudo apt install -y sshpass python3-pip

# Install ansible via pip
pip:
	@sudo pip3 install --upgrade pip
	@sudo pip3 install ansible

# install requirements.yml file
reqs:
	@ansible-galaxy install -r requirements.yml

# Store your password for use with the playbook commands and if the vault is encrypted
# Python is just 1000% better at parsing raw data than bash/GNU Make. /rant
store-password:
	@red=`tput setaf 1`
	@green=`tput setaf 2`
	@reset=`tput sgr0`
	@if [ -n "$${VAULT_PASS}" ]; then\
		if [ ! -d ~/.ansible ]; then\
			mkdir ~/.ansible;\
		fi;\
		echo "$${VAULT_PASS}" > ~/.ansible/password;\
		if [ ! "$${VAULT_PASS}" = "$$(cat ~/.ansible/password)" ]; then\
			echo "$$(tput setaf 1)PASSWORD WAS NOT ABLE TO UPDATE! Please manually invoke the custom python script to do this for you as follows:";\
			echo "./bin/parse_pass.py 'super_secret_password' <- Make sure to use single quotes.$$(tput sgr0)";\
		else\
			echo "$$(tput setaf 2)PASSWORD SUCCESSFULLY STORED IN '~/.ansible/password'!$$(tput sgr0)";\
		fi;\
	fi

# Creates a pre-commit webhook so that you don't accidentally commit decrypted vault upstream
githook:
	@./bin/git_init.sh

# Decrypt all files in this repo
decrypt:
	ansible-vault decrypt --vault-password-file ~/.ansible/password group_vars/all.yml
	ansible-vault decrypt --vault-password-file ~/.ansible/password inventory

# Encrypt all files in this repo
encrypt:
	ansible-vault encrypt --vault-password-file ~/.ansible/password group_vars/all.yml
	ansible-vault encrypt --vault-password-file ~/.ansible/password inventory
